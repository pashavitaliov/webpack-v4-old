import React from 'react';
import { render} from 'react-dom';
import App from './App';
import './css/styl';

render(
    <div>
      <App />
    </div>,
    document.getElementById('app'),
);
