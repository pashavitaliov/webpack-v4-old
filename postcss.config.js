//путь до из webpack.config до postcss.config определяется
// автоматически если он лежит в корне проекта


module.exports = {
  plugins: [
    require('autoprefixer')({
      'browsers': ['> 1%', 'last 4 versions']
    })
  ]
};
