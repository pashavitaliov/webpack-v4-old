// просмотреть линтеры, их насройку, подключение, работоспособность
// пакеты в своих ветках в package.json
// линтр на git

const path = require('path');
const webpack = require('webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');

const removeEmpty = x => x.filter(y => !!y);

module.exports = (env, argv) => {
  const DEV_MODE = argv.mode === 'development';

  return {
    context: path.join(__dirname, 'src'),

    entry: './index',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js',
    },

    resolve: {
      modules: [path.join(__dirname, 'src'), 'node_modules'],
      extensions: ['.js', '.jsx', '.scss'],
    },

    module: {
      rules: [
        DEV_MODE ? {
          enforce: 'pre',
          test: /\.(js|jsx)?$/,
          exclude: '/node_modules/',
          loader: 'eslint-loader',
        } : {},
        {
          test: /\.(js|jsx)?$/,
          exclude: '/node_modules/',
          use: {
            loader: 'babel-loader',
          },
        }, {
          test: /\.(sa|sc|c)ss$/,
          use: [DEV_MODE ? 'style-loader' : MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
        }, {
          test: /\.(png|jpg|svg)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/images',
              name() {
                if (DEV_MODE) {
                  return '[name]-[hash:12].[ext]';
                }

                return '[name].[ext]';
              },
            },
          },
        }, {
          test: /\.(ttf|eot|woof|woof2)$/,
          exclude: [/node_modules/, /images/],
          use: {
            loader: 'url-loader',
            options: {
              outputPath: 'assets/fonts',
              name: '[name].[ext]',
              limit: 1024,
            },
          },
        }, {
          test: /\.ico$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                limit: 256,
              },
            },
          ],
        },
      ],
    },

    plugins: removeEmpty([
      new webpack.ProgressPlugin(),

      new HtmlWebpackPlugin({
        title: 'App',
        template: path.join(__dirname, 'src', 'index.html'),
        meta: { viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
        favicon: path.join(__dirname, 'src', 'favicon.ico'),
      }),

      !DEV_MODE ? new MiniCssExtractPlugin({ filename: 'css/styles.css' }) : false,
      DEV_MODE ? new StylelintWebpackPlugin() : new OptimizeCSSAssetsPlugin(),
    ]),

    devtool: DEV_MODE ? 'cheap-module-eval-source-map' : 'eval',

    devServer: {
      // contentBase: path.join(__dirname, '/'),
    },
  };
};
