# Webpack v4
---

##### Project builder, webpack v4.
The entry point is the **index.js** file in the **./src** directory.

# Getting Started
---
To get started, you need to install the dependencies and devDependencies
from **package.json** with the command.

```sh
$ cd webpack 4
$ npm install
```
And start the server.
```sh
$ npm run dev
```
